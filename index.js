console.log("I'm working :D"); // Testing if works

const express = require('express'); // To use express

require('./config/db');

const PORT = 3000; // PORT number
const server = express(); // Server config

const Porsche = require('./models/Porsche');

const router = express.Router(); // Router created

// CONFIGURING 1ST ROUTE
// Replaced server.use with router.get to get the elements from the server
router.get('/', (req, res) => {
    res.send('This is my server running with nodemon :D');
});

// CONFIGURING NEW ROUTE
// Replaced server.use with router.get to get the elements from the server
// Added params for query routes
// router.get('/series/:serie', (req, res) => {
//     const serieName = req.params.serie; // We save in a variable the value introduced in the url
//     const series = ['Misfits', 'Sense8', 'The Curse'];
//     const findSerieIndex = series.indexOf(serieName); // We look for the index were there's a coincidence
//     // If there's no index we return a message of no coincidence
//     if (findSerieIndex === -1) {
//         res.send('Serie not found');
//     }
//     // If there is such serie we return the response
//     res.send(series[findSerieIndex]);
// });

// CREATING ENDPOINT FOR THE DATABASE

// router.get('/porsche-base-model', (req, res) => {
//     return Porsche.find()
//         .then((porsche) => {
//             // IF WE FIND THE MODELS WE RETURN THEM TO THE USER
//             return res.status(200).json(porsche);
//         })
//         .catch((err) => {
//             // IF THERE'S AN ERROR, WE'LL SEND AN ERROR MESSAGE
//             return res.status(500).json(err);
//         });
// });

// CREATING THE SAME ENDPOINT WITH ASYNC/AWAIT

router.get('/porsche-base-model', async (req, res) => {
    try {
        const porsche = await Porsche.find();
        return res.status(200).json(porsche);
    } catch (err) {
        return res.status(500).json(err);
    }
});

// NEW ENDPOINT TO FIND PORSCHE BY ID

router.get('/porsche-base-model/:id', async (req, res) => {
    const id = req.params.id;
    try {
        const porsche = await Porsche.findById(id);
        if (porsche) {
            return res.status(200).json(porsche);
        } else {
            return res.status(404).json('No porsche found with this id');
        }
    } catch (err) {
        return res.status(500).json(err);
    }
});

// NEW ENDPOINT TO SEARCH BY NAME

router.get('/porsche-base-model/name/:name', async (req, res) => {
    const { name } = req.params;
    try {
        const porscheByName = await Porsche.find({ name: name });
        return res.status(200).json(porscheByName);
    } catch (err) {
        return res.status(500).json(err);
    }
});

// NEW ENDPOINT TO SEARCH BY SPECIFIC AGE (OR AGE GROUP)

router.get('/porsche-base-model/cv/:cv', async (req, res) => {
    const { cv } = req.params;
    try {
        const porscheByCv = await Porsche.find({ cv: { $gt: cv } });
        return res.status(200).json(porscheByCv);
    } catch (err) {}
    return res.status(500).json(err);
});

server.use('/', router); // We use the server.use instead of different server.use. All in one

// CREATING THE SERVER
server.listen(PORT, () => {
    console.log(`Server running at http://localhost:${PORT}`);
});
