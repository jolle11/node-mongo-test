// WE INITIALIZE MONGOOSE
const mongoose = require('mongoose');
// WE IMPORT OUR PER IN THIS FILE
const Porsche = require('../models/Porsche');
// WE CREATE OUR BASE MODELS
const porsche = [
    {
        name: '718 Cayman',
        cv: 300,
        price: 64846,
    },
    {
        name: '911 Carrera',
        cv: 385,
        price: 123288,
    },
    {
        name: 'Taycan',
        cv: 408,
        price: 87811,
    },
    {
        name: 'Panamera',
        cv: 330,
        price: 105683,
    },
    {
        name: 'Macan',
        cv: 265,
        price: 70255,
    },
    {
        name: 'Cayenne',
        cv: 340,
        price: 94327,
    },
];
// LAST, WE HAVE TO CREATE THE AN INSTANCE FOR EVERY PET WE HAVE IN OUR ARRAY.
// FOR IT, WE'LL USE THE new Porsche() method
const porscheDocuments = porsche.map((porsche) => new Porsche(porsche));
// NOW WE'LL CONNECT TO OUR DATABASE AND DISCONNECT AFTER THE INSERTION OF THE MODELS
mongoose
    .connect('mongodb://localhost:27017/porsche-base-model', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(async () => {
        // USING model.find() WE'LL OBTAIN AN ARRAY WITH ALL THE MODELS FROM THE DATABASE
        const allModels = await Porsche.find();
        // IF THE MODELS EXIST PREVIOUSLY, WE'LL DROP THE COLLECTION
        if (allModels.length) {
            await Porsche.collection.drop();
        }
    })
    .catch((err) => console.log(`Error deleting data: ${err}`))
    .then(async () => {
        // ONCE THE MODELS DATABASE IS EMPTY, WE'LL USE THE ARRAY porscheDocuments
        // TO FILL OUR DATABASE WITH ALL THE MODELS
        await Porsche.insertMany(porscheDocuments);
    })
    .catch((err) => console.log(`Error creating data: ${err}`))
    // TO FINISH, WE'LL DISCONNECT FROM THE DATABASE
    .finally(() => mongoose.disconnect());
