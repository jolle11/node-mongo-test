// INITIALIZE MONGOOSE
const mongoose = require('mongoose');
// INITIALIZE SCHEMA MODULE
const Schema = mongoose.Schema;
// CREATING THE SCHEMA
const porscheSchema = new Schema(
    {
        name: { type: String, required: true }, // REQUIRED MEANS THAT THE FIELD ITS OBLIGATORY
        cv: { type: Number },
        price: { type: Number },
    },
    {
        timestamps: true, // WITH TIMESTAMPS WE'LL SAVE THE DATES OF THE DOCUMENT UPDATES
    }
);
// NOW WE CREATE AND EXPORT THE PorscheBaseModel
const Porsche = mongoose.model('Porsche', porscheSchema);
module.exports = Porsche;
